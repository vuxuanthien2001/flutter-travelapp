import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_apptravel/cubit/app_cubit_state.dart';
import 'package:flutter_apptravel/cubit/app_cubits.dart';
import 'package:flutter_apptravel/pages/DetailPage.dart';
import 'package:flutter_apptravel/pages/WelcomePage.dart';
import 'package:flutter_apptravel/pages/navpage/HomePage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppCubitLogics extends StatefulWidget {
  const AppCubitLogics({Key? key}) : super(key: key);

  @override
  State<AppCubitLogics> createState() => _AppCubitLogicsState();
}

class _AppCubitLogicsState extends State<AppCubitLogics> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AppCubits, CubitStates>(
        builder: (context, state) {
          if (state is WelcomeState){
            return WelcomePage();
          } else if(state is LoadingState){
            return Center(child: CircularProgressIndicator(),);
          }
          else if(state is LoadedState){
            return HomePage();
          }
          else if(state is DetailState){
            return DetailPage();
          }
          else {
            return Container();
          }
        }),
    );
  }
}