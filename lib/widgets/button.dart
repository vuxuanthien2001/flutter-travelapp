import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_apptravel/misc/colors.dart';
import 'package:flutter_apptravel/widgets/text_normal.dart';

class ResponsiveButton extends StatelessWidget {

  bool? isResponsive;
  double? width;

  ResponsiveButton({Key? key, this.isResponsive, this.width = 120}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        height: 45,
        width: isResponsive == true ? double.maxFinite:width,
        decoration: BoxDecoration(
          
          borderRadius: BorderRadius.all(Radius.circular(30)),
          color: AppColors.mainColor
        ),
        child: Container(
          padding: const EdgeInsets.only(right: 20, left: 20),
          child: Row(
            mainAxisAlignment: isResponsive == true ? MainAxisAlignment.spaceBetween:MainAxisAlignment.center,
            children: [
              isResponsive == true ? Container(margin: EdgeInsets.only(left: 20), child: StyleText(text: "Book Trip Now", color: Colors.white)):Container(),
              Image.asset("assets/images/button-one.png")
            ],
          ),
        ),
      ),
    );
  }
}