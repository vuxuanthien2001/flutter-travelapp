import 'package:flutter/material.dart';
import 'package:flutter_apptravel/cubit/app_cubit_logics.dart';
import 'package:flutter_apptravel/cubit/app_cubits.dart';
import 'package:flutter_apptravel/pages/WelcomePage.dart';
import 'package:flutter_apptravel/services/data_services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
      ///home: WelcomePage()
      home: BlocProvider<AppCubits>(
        create: ((context) => AppCubits(
          data: DataServices(),
        )),
        child: AppCubitLogics(),),
    );
  }
}

