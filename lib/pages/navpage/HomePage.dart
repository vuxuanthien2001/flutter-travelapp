import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_apptravel/cubit/app_cubit_state.dart';
import 'package:flutter_apptravel/cubit/app_cubits.dart';
import 'package:flutter_apptravel/misc/colors.dart';
import 'package:flutter_apptravel/model/ExploreMore.dart';
import 'package:flutter_apptravel/pages/DetailPage.dart';
import 'package:flutter_apptravel/widgets/text_normal.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../widgets/text_bold.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  var images = {
    "assets/images/balloning.png": "Balloning",
    "assets/images/hiking.png": "Hiking",
    "assets/images/kayaking.png": "Kayaking",
    "assets/images/snorkling.png": "Snorkling",
    "assets/images/snorkling.png": "Snorkling"
  };

  List<ExploreMore> listExplore = [
    ExploreMore("assets/images/balloning.png", "Balloning"),
    ExploreMore("assets/images/hiking.png", "Hiking"),
    ExploreMore("assets/images/kayaking.png", "Kayaking"),
    ExploreMore("assets/images/snorkling.png", "Snorkling")
  ];

  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(length: 3, vsync: this);
    return Scaffold(body: BlocBuilder<AppCubits, CubitStates>(
      builder: (context, state) {
        if (state is LoadedState) {
          var info = state.places;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(top: 50, left: 20),
                child: Row(
                  // ignore: prefer_const_literals_to_create_immutables
                  children: [
                    const Icon(
                      Icons.menu,
                      size: 30,
                      color: Colors.black,
                    ),
                    Expanded(child: Container()),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: const DecorationImage(
                              image: NetworkImage(
                                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyv04EkH6F3uKG4ygZR6DmxQWvG3pIc_XEJw&usqp=CAU"))),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: StyleTextBold(text: "Discover")),
              const SizedBox(
                height: 15,
              ),
              // ignore: avoid_unnecessary_containers
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: TabBar(
                      labelPadding: const EdgeInsets.only(left: 20, right: 20),
                      controller: _tabController,
                      labelColor: Colors.black,
                      unselectedLabelColor: Colors.grey,
                      isScrollable: true,
                      indicatorSize: TabBarIndicatorSize.label,
                      indicator: CircleTabIndicator(
                          color: AppColors.mainColor, radius: 5),
                      // ignore: prefer_const_literals_to_create_immutables
                      tabs: [
                        const Tab(
                          text: "Places",
                        ),
                        const Tab(
                          text: "Inspriration",
                        ),
                        const Tab(
                          text: "Emotions",
                        ),
                      ]),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 20),
                height: 300,
                width: double.maxFinite,
                child: TabBarView(controller: _tabController, children: [
                  Expanded(
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: info.length,
                      itemBuilder: ((context, index) {
                        return InkWell(
                          child: Container(
                            margin: const EdgeInsets.only(right: 10, top: 10),
                            height: 300,
                            width: 200,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                image: DecorationImage(
                                    image: NetworkImage(
                                        "http://mark.bslmeiyu.com/uploads/" +
                                            info[index].img),
                                    fit: BoxFit.cover)),
                          ),
                          onTap: () {
                            BlocProvider.of<AppCubits>(context).detailPage(info[index]);

                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(
                            //     builder: (context) => const DetailPage(),
                            //   ),
                            // );
                          },
                        );
                      }),
                    ),
                  ),
                  Container(
                    color: Colors.red,
                  ),
                  Container(
                    color: Colors.blue,
                  ),
                ]),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    StyleTextBold(
                      text: "Explore more",
                      size: 20,
                    ),
                    StyleText(
                      text: "See all",
                      color: AppColors.textColor1,
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 110,
                width: double.maxFinite,
                child: Expanded(
                    child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 4,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: const EdgeInsets.only(left: 20),
                      child: Expanded(
                        child: Column(
                          children: [
                            Container(
                              height: 80,
                              width: 80,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          images.keys.elementAt(index)),
                                      fit: BoxFit.cover)),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            StyleText(
                              text: images.values.elementAt(index),
                              color: AppColors.textColor2,
                            )
                          ],
                        ),
                      ),
                    );
                  },
                )),
              ),
            ],
          );
        } else {
          return Container();
        }
      },
    ));
  }
}

class CircleTabIndicator extends Decoration {
  final Color color;
  double radius;
  CircleTabIndicator({required this.color, required this.radius});
  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    // TODO: implement createBoxPainter
    return _CirclePainter(color: color, radius: radius);
  }
}

class _CirclePainter extends BoxPainter {
  final Color color;
  double radius;
  _CirclePainter({required this.color, required this.radius});

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    // TODO: implement paint
    Paint _paint = Paint();
    _paint.color = color;
    _paint.isAntiAlias = true;
    final Offset circleOffset = Offset(
        configuration.size!.width / 2, configuration.size!.height - radius);
    canvas.drawCircle(offset + circleOffset, radius, _paint);
  }
}
