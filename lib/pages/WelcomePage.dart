import 'package:flutter/material.dart';
import 'package:flutter_apptravel/cubit/app_cubits.dart';
import 'package:flutter_apptravel/misc/colors.dart';
import 'package:flutter_apptravel/widgets/button.dart';
import 'package:flutter_apptravel/widgets/text_bold.dart';
import 'package:flutter_apptravel/widgets/text_normal.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'MainPage.dart';

// ignore: must_be_immutable
class WelcomePage extends StatelessWidget {
  List listImages = [
    "assets/images/welcome-one.png",
    "assets/images/welcome-two.png",
    "assets/images/welcome-three.png"
  ];

  List listText = [
    "Travel",
    "Mountain",
    "Mountain hikes give you an incredible sense of freedomalong with endurance test"
  ];

  WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView.builder(
            itemCount: listImages.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              return Container(
                width: double.maxFinite,
                height: double.maxFinite,
                // ignore: prefer_interpolation_to_compose_strings
                decoration: BoxDecoration(
                    image: DecorationImage(
                        // ignore: prefer_interpolation_to_compose_strings
                        image: AssetImage(listImages[index]),
                        fit: BoxFit.fill)),
                child: Container(
                  margin: const EdgeInsets.only(top: 100, left: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          StyleTextBold(text: listText[0]),
                          StyleText(
                            text: listText[1],
                            size: 28,
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          SizedBox(
                            width: 250,
                            child: StyleText(
                              text: listText[2],
                            ),
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                          InkWell(
                            child: ResponsiveButton(
                              width: 126,
                              isResponsive: false,
                            ),
                            onTap: () {
                              BlocProvider.of<AppCubits>(context).getData();
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //     builder: (context) => const MainPage(),
                              //   ),
                              // );
                            },
                          )
                        ],
                      ),
                      Column(
                        children: List.generate(3, (indexSlide) {
                          return Container(
                            margin: const EdgeInsets.only(bottom: 5, right: 15),
                            height: index == indexSlide ? 50 : 10,
                            width: 10,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: index == indexSlide
                                    ? AppColors.mainColor
                                    : AppColors.mainColor.withOpacity(0.3)),
                          );
                        }),
                      )
                    ],
                  ),
                ),
              );
            }));
  }
}
