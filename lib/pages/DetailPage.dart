import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_apptravel/cubit/app_cubit_state.dart';
import 'package:flutter_apptravel/cubit/app_cubits.dart';
import 'package:flutter_apptravel/misc/colors.dart';
import 'package:flutter_apptravel/widgets/app_button.dart';
import 'package:flutter_apptravel/widgets/button.dart';
import 'package:flutter_apptravel/widgets/text_bold.dart';
import 'package:flutter_apptravel/widgets/text_normal.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  int gottenStar = 4;
  int selectIndex = -1;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppCubits, CubitStates>(builder: (context, state) {
      DetailState detailState = state as DetailState;
      return Scaffold(
        body: Container(
          height: double.maxFinite,
          width: double.maxFinite,
          child: Stack(
            children: [
              Positioned(
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 300,
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(
                                        "http://mark.bslmeiyu.com/uploads/" + detailState.places.img),
                            fit: BoxFit.cover)),
                  )),
              Container(
                margin: const EdgeInsets.only(top: 30, left: 10, right: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Positioned(
                        left: 20,
                        top: 40,
                        child: Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                BlocProvider.of<AppCubits>(context).goHome();
                              },
                              icon: Icon(Icons.menu),
                              color: Colors.white,
                            ),
                          ],
                        )),
                    Positioned(
                        left: 20,
                        top: 40,
                        child: Row(
                          children: [
                            IconButton(
                              onPressed: () {},
                              icon: Icon(Icons.more_vert),
                              color: Colors.white,
                            ),
                          ],
                        )),
                  ],
                ),
              ),
              Positioned(
                top: 250,
                child: Container(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  width: MediaQuery.of(context).size.width,
                  height: 500,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          StyleTextBold(text: detailState.places.name),
                          StyleTextBold(
                            text: "\$ " + detailState.places.price.toString(),
                            color: AppColors.textColor1,
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: AppColors.mainColor,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          StyleText(
                            text: detailState.places.location,
                            color: AppColors.textColor1,
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Wrap(
                            children: List.generate(5, (index) {
                              return Icon(Icons.star,
                                  color: index < detailState.places.stars
                                      ? AppColors.starColor
                                      : AppColors.textColor2);
                            }),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          StyleText(
                            text: "(5.0)",
                            color: AppColors.textColor2,
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      StyleTextBold(
                        text: "People",
                        size: 20,
                        color: Colors.black,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      StyleText(
                        text: "Number of people in your group",
                        color: AppColors.mainTextColor,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: List.generate(5, (index) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                selectIndex = index;
                              });
                            },
                            child: Container(
                              margin: const EdgeInsets.only(right: 10),
                              child: AppButtons(
                                color: selectIndex == index
                                    ? Colors.white
                                    : Colors.black,
                                backgroundColor: selectIndex == index
                                    ? Colors.black
                                    : AppColors.buttonBackground,
                                size: 50,
                                borderColor: selectIndex == index
                                    ? Colors.black
                                    : AppColors.buttonBackground,
                                text: (index + 1).toString(),
                              ),
                            ),
                          );
                        }),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      StyleTextBold(
                        text: "Description",
                        size: 20,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      StyleText(
                        text:
                            detailState.places.description,
                        color: AppColors.mainTextColor,
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                left: 20,
                bottom: 40,
                right: 20,
                child: Row(
                  children: [
                    AppButtons(
                      color: AppColors.textColor1,
                      backgroundColor: Colors.white,
                      size: 45,
                      borderColor: AppColors.textColor1,
                      isIcon: true,
                      icon: Icons.favorite_border,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    ResponsiveButton(isResponsive: true)
                  ],
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
